Name: gnome-shell-extension-extend-left-box-git
Version: 4.f1a8893
Release: 1
Summary: 'Extend left box of the GNOME Shell top panel.'
License: unknown
Requires: gnome-shell
BuildRequires: git
#Source0: git://github.com/StephenPCG/extend-left-box.git
%define __brp_mangle_shebangs %{nil}
%define debug_package %{nil}
%define srcdir %{_builddir}
%global _gitname extend-left-box
%define pkgname gnome-shell-extension-extend-left-box-git

%prep
 

%description
'Extend left box of the GNOME Shell top panel.'

%install
    export pkgdir="%{buildroot}"
    export srcdir="%{srcdir}"
    export pkgname="%{pkgname}"
    export _pkgname="%{_pkgname}"
    export _pkgfolder="%{_pkgfolder}"
    export pkgver="%{version}"  
    cd "%{_builddir}"               
    install -dm755 "$pkgdir/usr/share/gnome-shell/extensions";
    cp -av "$srcdir/%{_gitname}/extend-left-box@linuxdeepin.com" "$pkgdir/usr/share/gnome-shell/extensions"

%files
/*
%exclude %dir /usr/bin
%exclude %dir /usr/lib

